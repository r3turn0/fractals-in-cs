#include <iostream>
#include <stdlib.h>
#include <GL/glut.h>
#include <math.h>

#define KEY_ESCAPE 27
#define PI 3.141592

#include <unistd.h>

using namespace std;

int I, Shape;

typedef struct {
  int width;
  int height;
  char* title;

  float field_of_view_angle;
  float z_near;
  float z_far;
} glutWindow;

glutWindow win;

void drawTri(GLfloat x, GLfloat y, int iter, int scale, int shape){
  if(iter==0){
    double radius=pow(0.5, scale+1);
    switch(shape){
    case 1:
      glBegin(GL_TRIANGLES);
      glColor3f(0.0f,1.0f,0.0f);
      glVertex3f(x,                  y,                0.0f);
      glVertex3f(x+pow(0.5,scale)  , y,                0.0f);
      glVertex3f(x+pow(0.5,scale+1), y+pow(0.5,scale), 0.0f);
      glEnd();
      break;
    case 2:
      glBegin(GL_QUADS);
      glColor3f(0.0f,0.0f,1.0f);
      glVertex3f(x                , y                , 0.0f);
      glVertex3f(x+pow(0.5, scale), y                , 0.0f);
      glVertex3f(x+pow(0.5, scale), y+pow(0.5, scale), 0.0f);
      glVertex3f(x                , y+pow(0.5, scale), 0.0f);
      glEnd();
      break;
    case 3:
      glBegin(GL_POLYGON);
      glColor3f(1.0f, 0.0f,0.0f);
      for(double i=0; i<2*PI; i+=PI/6){
        glVertex3f(x+cos(i)*radius, y+sin(i)*radius, 0.0f);
      }
      glEnd();
      break;
    case 4:
      glBegin(GL_QUADS);
      glColor3f(0.0f,0.0f,1.0f);
      glVertex3f(x                , y                , 0.0f);
      glVertex3f(x+pow(0.5, scale), y                , 0.0f);
      glVertex3f(x+pow(0.5, scale), y+pow(0.5, scale), 0.0f);
      glVertex3f(x                , y+pow(0.5, scale), 0.0f);
      glEnd();

      glBegin(GL_TRIANGLES);
      glColor3f(0.0f,1.0f,0.0f);
      glVertex3f(x,                  y,                0.0f);
      glVertex3f(x+pow(0.5,scale)  , y,                0.0f);
      glVertex3f(x+pow(0.5,scale+1), y+pow(0.5,scale), 0.0f);
      glEnd();
   
      glBegin(GL_POLYGON);
      glColor3f(1.0f, 0.0f,0.0f);
      for(double i=0; i<2*PI; i+=PI/6){
        glVertex3f(x+cos(i)*radius, y+sin(i)*radius, 0.0f);
      }
      glEnd();
      break;
      
    default:
      break;
    }//end of switch
  }//end of iter=0 case
  else{
    drawTri(x                , y              , iter-1, scale, shape);
    drawTri(x+pow(0.5,iter)  , y              , iter-1, scale, shape);
    drawTri(x+pow(0.5,iter+1), y+pow(0.5,iter), iter-1, scale, shape);
  }
};

void Gasket(int iter, int shape){
  drawTri(-1.0f,-1.0f,iter,iter,shape);
  drawTri(-0.5f, 0.0f,iter,iter,shape);
  drawTri( 0.0f,-1.0f,iter,iter,shape);
};

void awesome_gasket(){
  int start_depth = 1,
    stop_depth = 8,
    shape = 3,
    pause = 750000;

  for (int depth = start_depth; depth <= stop_depth; depth++){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Clear Screen and Depth Buffer
    glLoadIdentity();
    glTranslatef(0.0f,0.0f,-3.0f);
    Gasket(depth, Shape);
    glutSwapBuffers();
    usleep(pause);
  }
}

void display(){
  awesome_gasket();
  return;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  // Clear Screen and Depth Buffer
  glLoadIdentity();
  glTranslatef(0.0f,0.0f,-3.0f);

  Gasket(I,Shape);

  glutSwapBuffers();
}

void initialize (){
  glMatrixMode(GL_PROJECTION);// select projection matrix
  glViewport(0, 0, win.width, win.height);// set the viewport
  glMatrixMode(GL_PROJECTION);// set matrix mode
  glLoadIdentity();// reset projection matrix
  GLfloat aspect = (GLfloat) win.width / win.height;
  gluPerspective(win.field_of_view_angle, aspect, win.z_near, win.z_far);
  // set up a perspective projection matrix
  glMatrixMode(GL_MODELVIEW);// specify which matrix is the current matrix
  glShadeModel( GL_SMOOTH );
  glClearDepth( 1.0f );// specify the clear value for the depth buffer
  glEnable( GL_DEPTH_TEST );
  glDepthFunc( GL_LEQUAL );
  glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );// specify implementation-specific hints
  glClearColor(0.0, 0.0, 0.0, 1.0);// specify clear values for the color buffers
}


void keyboard ( unsigned char key, int mousePositionX, int mousePositionY ){
  switch ( key ){
  case KEY_ESCAPE:
    exit ( 0 );
    break;
  default:
    break;
  }
}

int main(int argc, char **argv) { 
  cout<< "Which shape would you like to use?"
      << "\n1. Triangle"
      << "\n2. Square"
      << "\n3. Circle"
      << "\n4. All three shapes overlapping"
      << endl;
  cin>>Shape;
  
  // set window values
  win.width = 640;
  win.height = 480;
  win.title = "Fractal Geometry Joe and John";
  win.field_of_view_angle = 45;
  win.z_near = 1.0f;
  win.z_far = 500.0f;

  // initialize and run program
  glutInit(&argc, argv);// GLUT initialization
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );// Display Mode
  glutInitWindowSize(win.width,win.height);// set window size
  glutCreateWindow(win.title);// create Window
  glutDisplayFunc(display);// register Display Function
  glutIdleFunc( display );// register Idle Function
  glutKeyboardFunc( keyboard );// register Keyboard Handler
  initialize();
  glutMainLoop();// run GLUT mainloop
  return 0;
}
